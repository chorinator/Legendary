﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Legendary.AmazonServices
{
   public class Enums
   {
      public enum Zone
      {
         USWest2 = 1,
      }

      public enum PictureType
      {
         None = 0,
         FingerPrint = 1,
         Photo = 2,
      }
   }
}
