﻿using System;

namespace Legendary.Web.Exceptions
{
   public class UnauthorizedException : HttpStatusCodeException
   {
      public UnauthorizedException(string message, Exception inner)
         : base(message, inner)
      {
      }
   }
}