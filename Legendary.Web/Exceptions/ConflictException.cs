﻿using System;

namespace Legendary.Web.Exceptions
{
   public class ConflictException : HttpStatusCodeException
   {
      public ConflictException(string message, Exception inner)
         : base(message, inner)
      {
      }
   }
}