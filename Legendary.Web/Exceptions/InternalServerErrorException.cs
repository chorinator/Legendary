﻿using System;

namespace Legendary.Web.Exceptions
{
   public class InternalServerErrorException : HttpStatusCodeException
   {
      public InternalServerErrorException(string message, Exception inner)
         : base(message, inner)
      {
      }
   }
}