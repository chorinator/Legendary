﻿using System;

namespace Legendary.Web.Exceptions
{
   public class NotFoundException : HttpStatusCodeException
   {
      public NotFoundException(string message, Exception inner)
         : base(message, inner)
      {
      }
   }
}