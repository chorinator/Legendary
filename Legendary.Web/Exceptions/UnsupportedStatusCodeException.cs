﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Legendary.Web.Exceptions
{
   public class UnsupportedStatusCodeException : HttpStatusCodeException
   {
      public UnsupportedStatusCodeException(string message, Exception inner) : base(message, inner)
      {
      }
   }
}
