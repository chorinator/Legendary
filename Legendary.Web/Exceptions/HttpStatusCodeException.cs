﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Legendary.Web.Exceptions
{
   public abstract class HttpStatusCodeException : Exception
   {
      protected HttpStatusCodeException(string message, Exception inner)
         : base(message, inner)
      { }
   }
}
