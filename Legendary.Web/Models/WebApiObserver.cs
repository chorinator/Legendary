﻿using System;
using System.Web.Mvc;

namespace Legendary.Web.Models
{
   public class WebApiObserver
   {
      public string Url { get; set; }
      public string ApiKey { get; set; }
      public string TypeName { get; set; }
      public HttpVerbs HttpVerb { get; set; }

      public WebApiObserver()
      {
         HttpVerb = HttpVerbs.Post;
      }
      public override int GetHashCode()
      {
         return (Url + ApiKey + TypeName + HttpVerb).GetHashCode();
      }
   }
}