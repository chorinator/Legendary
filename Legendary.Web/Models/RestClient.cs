﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;
using Legendary.Cache;
using Legendary.Cache.Models;
using Legendary.Core.Model;
using Legendary.Core.Model.Request;
using Legendary.Core.Utils;
using System.Data.Entity.Design.PluralizationServices;

namespace Legendary.Web.Models
{
   public abstract class RestClient
   {
      protected readonly RestServices Rest;
      protected string Resource;

      protected RestClient(string resource, RestServices rest)
      {
         Resource = resource;
         Rest = rest;
      }
   }

   public class RestClient<T> : RestClient
   {
      public bool IgnoreCache { get; set; }

      public RestClient(string resource, RestServices rest, bool ignoreCache = false)
         : base(resource, rest)
      {
         IgnoreCache = ignoreCache;
      }

      /// <summary>
      /// Utiliza los estándares de VS para obtener el recurso.
      /// Solo utilizar esta sobrecarga si se siguieron todas las convenciones al momento de crear el API
      /// </summary>
      /// <param name="rest">El servicio REST para comunicarse con el API</param>
      public RestClient(RestServices rest)
         : this(PluralizationService.CreateService(new CultureInfo("en-us")).Pluralize(typeof(T).Name), rest)
      { }

      public virtual IEnumerable<T> Get()
      {
         var cacheRequest = new CacheRequest
         {
            Key = MethodBase.GetCurrentMethod().Name + Rest.BaseUri + Resource,
            Callback = () => Rest.GetResource<IEnumerable<T>>(Resource),
            IgnoreCache = IgnoreCache,
         };
         return IgnoreCache
            ? Rest.GetResource<IEnumerable<T>>(Resource)
            : CacheManager.Cache.Get<IEnumerable<T>>(cacheRequest);
      }

      public virtual T Get(int id)
      {
         var cacheRequest = new CacheRequest
         {
            Key = MethodBase.GetCurrentMethod().Name + Rest.BaseUri + Resource + id,
            Callback = () => Rest.GetResource<T>(Resource + "/{0}".FormatWith(id)),
            IgnoreCache = IgnoreCache,
         };

         return IgnoreCache
            ? Rest.GetResource<T>(Resource + "/{0}".FormatWith(id))
            : CacheManager.Cache.Get<T>(cacheRequest);
      }

      public virtual Paginable<T> Get(PaginatedRequest request)
      {
         var url = request.UrlEncode();
         var cacheRequest = new CacheRequest
         {
            Key = MethodBase.GetCurrentMethod().Name + Rest.BaseUri + Resource + url,
            Callback = () => Rest.GetResource<Paginable<T>>(Resource + "/?request={0}".FormatWith(url)),
            IgnoreCache = IgnoreCache,
         };

         return IgnoreCache
            ? Rest.GetResource<Paginable<T>>(Resource + "/?request={0}".FormatWith(url))
            : CacheManager.Cache.Get<Paginable<T>>(cacheRequest);
      }

      public virtual T Save(T item)
      {
         var result = Rest.PostResource<T>(Resource, item);
         CacheManager.Cache.Clear();

         return result;
      }

      public virtual T Update(int id, T item)
      {
         var result = Rest.PutResource<T>(Resource + "/{0}".FormatWith(id), item);
         CacheManager.Cache.Clear();

         return result;
      }

      public virtual void Delete(int id)
      {
         Rest.DeleteResource<T>(Resource + "/{0}".FormatWith(id));
         CacheManager.Cache.Clear();
      }

      #region Async

      public async Task<T> GetAsAsync(int id)
      {
         return await Task.Factory.StartNew(() => Get(id));
      }

      public async Task<IEnumerable<T>> GetAsAsync()
      {
         return await Task.Factory.StartNew(() => Get());
      }

      public async Task<Paginable<T>> GetAsAsync(PaginatedRequest request)
      {
         return await Task.Factory.StartNew(() => Get(request));
      }

      public async void SaveAsAsync(T item)
      {
         await Task.Factory.StartNew(() => Save(item));
      }

      public async void UpdateAsAsync(int id, T item)
      {
         await Task.Factory.StartNew(() => Update(id, item));
      }

      public async void DeleteAsAsync(int id)
      {
         await Task.Factory.StartNew(() => Delete(id));
      }

      #endregion

   }
}