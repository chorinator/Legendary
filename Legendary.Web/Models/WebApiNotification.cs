using System;

namespace Legendary.Web.Models
{
   public class WebApiNotification
   {
      public string TypeName { get; set; }
      public string ObjectToNotify { get; set; }
   }
}