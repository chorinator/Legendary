using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;

namespace Legendary.Web.Models
{
   /// <summary>
   /// Permite usar servicios REST con autenticaci�n
   /// </summary>
   public class RestServicesWithAuthentication : RestServices
   {
      private readonly string _authKey;
      private readonly string _authType;

      /// <summary>
      /// Constructor
      /// </summary>
      /// <param name="baseUri">Uri Base del servicio REST</param>
      /// <param name="authKey">Llave de autenticacion</param>
      /// <param name="authType">Si no se especifica alg�n tipo de autenticaci�n se usar� "Basic"</param>
      public RestServicesWithAuthentication(string baseUri, string authKey, string authType = "Basic")
         : base(baseUri)
      {
         _authKey = String.IsNullOrWhiteSpace(authKey) ? String.Empty : authKey;
         _authType = String.IsNullOrWhiteSpace(authType) ? "Basic" : authType;
      }

      public RestServicesWithAuthentication(string authKey)
         : this(String.Empty, authKey) { }

      protected override void AddHeadersToHttpClient(HttpClient client, List<KeyValuePair<string, string>> headers)
      {
         base.AddHeadersToHttpClient(client, headers);
         client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(_authType, _authKey);
      }
   }
}