﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Authentication;
using System.Threading.Tasks;
using System.Web.Mvc;
using Legendary.Core.Utils;
using Legendary.Web.Exceptions;
using Newtonsoft.Json;

namespace Legendary.Web.Models
{
   /// <summary>
   /// Facilita las llamadas REST entre Cliente y API.
   /// Debe crearse una instancia de RestServices por cada BaseUri.
   /// 
   /// Información importante para generar sus ResourceUri y BaseUris:
   /// http://stackoverflow.com/questions/23438416/why-is-httpclient-baseaddress-not-working
   /// </summary>
   public class RestServices
   {
      internal string BaseUri;
      public TimeSpan Timeout { get; set; }

      public RestServices(string baseUri)
      {
         BaseUri = baseUri;
         Timeout = new TimeSpan(0, 0, 200);
      }

      public RestServices()
         : this(String.Empty)
      { }

      private T GetResponse<T>(HttpVerbs verb, string resourceUri, object obj,
         List<KeyValuePair<string, string>> headers)
      {
         HttpResponseMessage response;

         using (var handler = new HttpClientHandler())
         {
            using (var client = new HttpClient(handler) { Timeout = Timeout })
            {
               if (!String.IsNullOrWhiteSpace(BaseUri))
                  client.BaseAddress = new Uri(BaseUri);

               AddHeadersToHttpClient(client, headers);

               switch (verb)
               {
                  case HttpVerbs.Get:
                     response = client.GetAsync(resourceUri).Result;
                     break;

                  case HttpVerbs.Post:
                     response = client.PostAsJsonAsync(resourceUri, obj).Result;
                     break;

                  case HttpVerbs.Put:
                     response = client.PutAsJsonAsync(resourceUri, obj).Result;
                     break;

                  case HttpVerbs.Delete:
                     response = client.DeleteAsync(resourceUri).Result;
                     break;

                  default:
                     throw new NotImplementedException("HttpVerb {0} not implemented".FormatWith(verb.ToString()));
               }

               if (response.IsSuccessStatusCode)
               {
                  return response.Content.ReadAsAsync<T>().Result;
               }
            }
         }


         var message = String.IsNullOrWhiteSpace(BaseUri)
            ? String.Format("StatusCode: {0}, Verb: {1}, Address: {2}", response.StatusCode, verb, resourceUri)
            : String.Format("StatusCode: {0}, Verb: {1}, Base address: {2} Resource: {3}", response.StatusCode, verb, BaseUri, resourceUri);

         var ex = HttpStatusCodeExceptionSimpleFactory.CreateException(response, new Exception(message));

         throw ex;
      }

      /// <summary>
      /// Hace una llamada GET
      /// </summary>
      /// <param name="resourceUri">Uri del servicio</param>
      /// <typeparam name="T"></typeparam>
      /// <returns>Convierte el HttpResponseMessage al objeto T</returns>
      public T GetResource<T>(string resourceUri)
      {
         return GetResponse<T>(HttpVerbs.Get, resourceUri, null, null);
      }

      /// <summary>
      /// Hace una llamada DELETE
      /// </summary>
      /// <param name="resourceUri">Uri del servicio</param>
      /// <typeparam name="T"></typeparam>
      /// <returns>Convierte el HttpResponseMessage al objeto T</returns>
      public T DeleteResource<T>(string resourceUri)
      {
         return GetResponse<T>(HttpVerbs.Delete, resourceUri, null, null);
      }

      /// <summary>
      /// Hace una llamada POST
      /// </summary>
      /// <typeparam name="T"></typeparam>
      /// <param name="resourceUri">Uri del servicio</param>
      /// <param name="obj">El objeto a POSTear</param>
      /// <param name="headers">Headers HTTP opcionales para el request</param>
      /// <returns>Convierte el HttpResponseMessage al objeto T</returns>
      public T PostResource<T>(string resourceUri, object obj, List<KeyValuePair<string, string>> headers = null)
      {
         return GetResponse<T>(HttpVerbs.Post, resourceUri, obj, headers);
      }

      /// <summary>
      /// Hace una llamada POST
      /// </summary>
      /// <typeparam name="T"></typeparam>
      /// <param name="resourceUri">Uri del servicio</param>
      /// <param name="obj">El objeto a POSTear</param>
      /// <param name="headers">Headers HTTP opcionales para el request</param>
      /// <returns>Convierte el HttpResponseMessage al objeto T</returns>
      public T PutResource<T>(string resourceUri, object obj, List<KeyValuePair<string, string>> headers = null)
      {
         return GetResponse<T>(HttpVerbs.Put, resourceUri, obj, headers);
      }

      public async Task<T> GetResourceAsSync<T>(string resourceUri)
      {
         return await Task<T>.Run(() => GetResource<T>(resourceUri));
      }

      public async Task<T> PostResourceAsSync<T>(string resourceUri, object obj)
      {
         return await Task<T>.Run(() => PostResource<T>(resourceUri, obj));
      }

      public async Task<T> PutResourceAsSync<T>(string resourceUri, object obj)
      {
         return await Task<T>.Run(() => PutResource<T>(resourceUri, obj));
      }

      public async Task<T> DeleteResourceAsSync<T>(string resourceUri)
      {
         return await Task<T>.Run(() => DeleteResource<T>(resourceUri));
      }

      /// <summary>
      /// Agrega los headers que indiquen las clases derivadas y los que mande el usuario
      /// </summary>
      /// <param name="client">HttpClient en uso</param>
      /// <param name="headers">Headers HTTP opcionales</param>
      protected virtual void AddHeadersToHttpClient(HttpClient client, List<KeyValuePair<string, string>> headers)
      {
         client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
      }
   }
}