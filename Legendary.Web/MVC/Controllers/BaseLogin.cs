﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Legendary.Core.Utils;

namespace Legendary.Web.MVC.Controllers
{
    public abstract class BaseLogin<T> : Controller where T : new()
    {
        [HttpGet]
        public ActionResult Login(string absoluteUri)
        {
            return RedirectResult(new T(), LoginActionName, absoluteUri, false);
        }

        [ValidateAntiForgeryToken, HttpPost]
        public ActionResult Login(T loginRequest, string absoluteUri)
        {
            if (ModelState.IsValid)
            {
                return RedirectResult(loginRequest, LoginActionName, absoluteUri, true);
            }
            else
                return View(loginRequest);
        }

        public ActionResult RedirectResult(T loginRequest, string viewName, string absoluteUri, bool tryLogin)
        {
            ViewBag.absolutUri = absoluteUri;
            var isLogged = IsLoggedIn();
            if (!isLogged)
            {
                if (tryLogin)
                {
                    SetSystem(loginRequest);
                    var result = LoginSession(loginRequest);
                    if (result)
                    {
                        Log(new { result = result.ToString() });
                        if (absoluteUri.IsNotEmptyOrNull())
                            return Redirect(absoluteUri);
                        else
                            return RedirectToAction(HomeActionName, HomeControllerName);
                    }
                    ViewBag.errorMessage = "Email o password inválidos";
                }
                return View(viewName, loginRequest);
            }
            if (absoluteUri.IsNotEmptyOrNull())
                return Redirect(absoluteUri);
            else
                return RedirectToAction(HomeActionName, HomeControllerName);
        }

        public ActionResult Logout()
        {
            LogoutSession();
            return RedirectToAction(LoginActionName, LoginControllerName);
        }

        public ActionResult ForgotPassword(string emailToSend)
        {
            ViewBag.success = false;
            if (!emailToSend.IsNotEmptyOrNull())
                return View("ForgotPassword", null, emailToSend);

            if (!emailToSend.IsValidEmail())
                return View("ForgotPassword", null, emailToSend);

            SendRememberMail(emailToSend);

            ViewBag.success = true;
            return View("ForgotPassword", null, emailToSend);
        }        

        public virtual string LoginActionName
        {
            get { return "Login"; }
        }
        public virtual string LoginControllerName
        {
            get { return "Account"; }
        }
        public virtual string HomeActionName
        {
            get { return "Index"; }
        }
        public virtual string HomeControllerName
        {
            get { return "Home"; }
        }

        public abstract void SendRememberMail(string emailToSend);
        public abstract bool IsLoggedIn();
        public abstract void SetSystem(T request);        
        public abstract bool LoginSession(T request);
        public abstract void LogoutSession();         
        public abstract void Log(object logObject); 
    }
}
