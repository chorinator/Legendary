﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Legendary.Web.MVC.Controllers
{
    public class BaseActions : Controller
    {
        public new RedirectToRouteResult RedirectToAction(string action, string controller, object routeValues = null)
        {
            return base.RedirectToAction(action, controller, routeValues);
        }
    }
}
