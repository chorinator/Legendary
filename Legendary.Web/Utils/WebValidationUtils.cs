﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.ModelBinding;

namespace Legendary.Web.Utils
{
   public static class WebValidationUtils
   {
      public static void AddToModelState(this IEnumerable<ValidationResult> validationResults,
         ModelStateDictionary modelState)
      {
         var resultsGroupedByMembers = validationResults
            .SelectMany(
               _ => _.MemberNames.Select(
                  x => new
                  {
                     MemberName = x ?? "",
                     Error = _.ErrorMessage
                  }))
            .GroupBy(_ => _.MemberName);

         foreach (var member in resultsGroupedByMembers)
         {
            modelState.AddModelError(
               member.Key,
               string.Join(". ", member.Select(_ => _.Error)));
         }
      }
   }
}
