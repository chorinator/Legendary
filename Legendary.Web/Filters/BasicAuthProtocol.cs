﻿using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http.Controllers;

namespace Legendary.Web.Filters
{
   internal class BasicAuthProtocol
   {
      /// <summary>
      /// Parses the Authorization header and creates user credentials
      /// </summary>
      /// <param name="actionContext"></param>
      public BasicAuthenticationIdentity ParseAuthorizationHeader(HttpActionContext actionContext, Func<string, string> decryptAuthHeader)
      {
         string authHeader = null;
         var auth = actionContext.Request.Headers.Authorization;
         if (auth != null && auth.Scheme == "Basic")
            authHeader = auth.Parameter;

         if (string.IsNullOrEmpty(authHeader))
            return null;

         authHeader = decryptAuthHeader(authHeader);

         var tokens = authHeader.Split(':');
         if (tokens.Length < 2)
            return null;

         return new BasicAuthenticationIdentity(tokens[0], tokens[1]);
      }

      /// <summary>
      /// Send the Authentication Challenge request
      /// </summary>
      /// <param name="actionContext"></param>
      public void Challenge(HttpActionContext actionContext)
      {
         var host = actionContext.Request.RequestUri.DnsSafeHost;
         actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized);
         actionContext.Response.Headers.Add("WWW-Authenticate", string.Format("Basic realm=\"{0}\"", host));
      }
   }
}