﻿using System;
using System.Web.Http.Controllers;

namespace Legendary.Web.Filters
{
   /// <summary>
   /// Generic Basic Authentication filter that checks for basic authentication
   /// headers and challenges for authentication if no authentication is provided
   /// Sets the Thread Principle with a GenericAuthenticationPrincipal.
   /// 
   /// You can override the OnAuthorize method for custom auth logic that
   /// might be application specific.    
   /// </summary>
   /// <remarks>Always remember that Basic Authentication passes username and passwords
   /// from client to server in plain text, so make sure SSL is used with basic auth
   /// to encode the Authorization header on all requests (not just the login).
   /// </remarks>
   [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false)]
   public abstract class BasicAuthorizationFilter : BasicAuthenticationFilter
   {
      private readonly BasicAuthProtocol _basicAuthProtocol;
      protected BasicAuthorizationFilter()
      {
         _basicAuthProtocol = new BasicAuthProtocol();
      }

      /// <summary>
      /// Override to Web API filter method to handle Basic Auth check
      /// </summary>
      /// <param name="actionContext"></param>
      public override void OnAuthorization(HttpActionContext actionContext)
      {
         base.OnAuthorization(actionContext);

         if (OnAuthorizeUser(Identity.Name, Identity.Password, actionContext)) 
            return;

         _basicAuthProtocol.Challenge(actionContext);
      }

      /// <summary>
      /// Base implementation for user authentication - you probably will
      /// want to override this method for application specific logic.
      /// 
      /// The base implementation merely checks for username and password
      /// present and set the Thread principal.
      /// 
      /// Override this method if you want to customize Authentication
      /// and store user data as needed in a Thread Principle or other
      /// Request specific storage.
      /// </summary>
      /// <param name="username"></param>
      /// <param name="password"></param>
      /// <param name="actionContext"></param>
      /// <returns></returns>
      protected abstract bool OnAuthorizeUser(string username, string password, HttpActionContext actionContext);
   }
}