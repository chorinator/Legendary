﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;

namespace Legendary.Web.Mobile
{
   /// <summary>
   /// Facilita las llamadas REST entre Cliente y API.
   /// Debe crearse una instancia de RestServices por cada BaseUri.
   /// 
   /// Información importante para generar sus ResourceUri y BaseUris:
   /// http://stackoverflow.com/questions/23438416/why-is-httpclient-baseaddress-not-working
   /// </summary>
   public class RestClient
   {
      internal string BaseUri;

      public RestClient(string baseUri)
      {
         BaseUri = baseUri;
      }

      private T GetResponse<T>(string verb, string resource, object obj)
      {
         // Create an HTTP web request using the URL:
         var uri = new Uri(BaseUri + resource);
         var client = new HttpClient();
         client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
         HttpResponseMessage response;
         var json = new StringContent(JsonConvert.SerializeObject(obj));

         switch (verb)
         {
            case "GET":
               response = client.GetAsync(uri).Result;
               break;
            case "DELETE":
               response = client.DeleteAsync(uri).Result;
               break;
            case "POST":
               response = client.PostAsync(uri, json).Result;
               break;
            case "PUT":
               response = client.PutAsync(uri, json).Result;
               break;

            default:
               throw new NotImplementedException(verb);
         }
         if (!response.IsSuccessStatusCode)
            throw new ArgumentException(response.StatusCode.ToString());

         var result = response.Content.ReadAsStringAsync().Result;
         return JsonConvert.DeserializeObject<T>(result);
      }

      /// <summary>
      /// Hace una llamada GET
      /// </summary>
      /// <param name="resourceUri">Uri del servicio</param>
      /// <typeparam name="T"></typeparam>
      /// <returns>Convierte el HttpResponseMessage al objeto T</returns>
      public T GetResource<T>(string resourceUri)
      {
         return GetResponse<T>("GET", resourceUri, null);
      }

      /// <summary>
      /// Hace una llamada DELETE
      /// </summary>
      /// <param name="resourceUri">Uri del servicio</param>
      /// <typeparam name="T"></typeparam>
      /// <returns>Convierte el HttpResponseMessage al objeto T</returns>
      public T DeleteResource<T>(string resourceUri)
      {
         return GetResponse<T>("DELETE", resourceUri, null);
      }

      /// <summary>
      /// Hace una llamada POST
      /// </summary>
      /// <typeparam name="T"></typeparam>
      /// <param name="resourceUri">Uri del servicio</param>
      /// <param name="obj">El objeto a POSTear</param>
      /// <param name="headers">Headers HTTP opcionales para el request</param>
      /// <returns>Convierte el HttpResponseMessage al objeto T</returns>
      public T PostResource<T>(string resourceUri, object obj)
      {
         return GetResponse<T>("POST", resourceUri, obj);
      }

      /// <summary>
      /// Hace una llamada POST
      /// </summary>
      /// <typeparam name="T"></typeparam>
      /// <param name="resourceUri">Uri del servicio</param>
      /// <param name="obj">El objeto a POSTear</param>
      /// <param name="headers">Headers HTTP opcionales para el request</param>
      /// <returns>Convierte el HttpResponseMessage al objeto T</returns>
      public T PutResource<T>(string resourceUri, object obj)
      {
         return GetResponse<T>("PUT", resourceUri, obj);
      }
   }
}