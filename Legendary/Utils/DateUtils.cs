﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Legendary.Core.Utils
{
   public static class DateUtils
   {
      public static bool IsNotMinValue(this DateTime dateTime)
      {
         return !dateTime.IsMinValue();
      }

      public static bool IsMinValue(this DateTime dateTime)
      {
         return dateTime <= DateTime.MinValue;
      }

      public static int GetYearsToDate(this DateTime date)
      {
         return DateTime.Now.Year - date.Year;
      }

      public static DateTime ToUTCDate(this DateTime date) {
          if (date.IsNotMinValue())
          {              
              return TimeZoneInfo.ConvertTimeToUtc(new DateTime(date.Ticks,DateTimeKind.Local), TimeZoneInfo.Local);
          }
          return date;
      }

       /// <summary>
       /// Returns a datetime in natural language eg. 2/ene/20xx
       /// </summary>
       /// <returns>string</returns>
      public static string ToFormattedString(this DateTime date, bool showHours = false, bool shortMonthName = false) { 
          return date.ToString("dd/MMM"+(shortMonthName?"":"M")+"/yyyy"+(showHours?", hh:mm tt":""));          
      }

      public static bool IsValidDate(this DateTime date)
      {
         return date > DateTime.MinValue && date < DateTime.MaxValue;
      }

      public static bool IsBetween(this DateTime obj, DateTime start, DateTime end)
      {
         return start <= obj && end >= obj;
      }
   }
}
