﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Legendary.Core.Model;
using Legendary.Core.Model.Request;
using System.Linq.Dynamic;

namespace Legendary.Core.Utils
{
   public static class QueryableUtils
   {

      public static Paginable<T> Paginate<T>(this IQueryable<T> items, PaginatedRequest<T> request)
      {
         return Paginate(items, request.Page, request.PageSize, request);
      }

      public static Task<Paginable<T>> PaginateAsync<T>(this IQueryable<T> items, PaginatedRequest<T> request)
      {
         return Task.Run(() => Paginate(items, request));
      }

      private static Paginable<T> Paginate<T>(this IQueryable<T> items, int? page, int? pageSize, PaginatedRequest<T> request)
      {
         if (!page.HasValue || page.Value <= 0)
            page = 1;

         if (!pageSize.HasValue || pageSize.Value <= 0)
            pageSize = 20;

         request.Page = page.Value;
         request.PageSize = pageSize.Value;
         request.TotalCount = items.Count();

         var funcs = request.ExecuteOtherFunctions(items);

         var ordered = request.OrderBy != null
            ? request.OrderBy.ExecuteRequest(items, request.OrderBy.ColumnName)
            : items.OrderBy(i => Guid.NewGuid());

         var paginable = ordered
            .Skip((page.Value - 1) * pageSize.Value)
            .Take(pageSize.Value)
            .AsPaginable(request);

         ((List<dynamic>)paginable.FunctionResults).AddRange(funcs);

         return paginable;
      }

      private static Paginable<T> AsPaginable<T>(this IQueryable<T> items, PaginatedRequest<T> request)
      {
         var page = request.Page.HasValue && request.Page.Value > 0
            ? request.Page.Value
            : 1;

         var pageSize = request.PageSize.HasValue && request.PageSize.Value > 0
            ? request.PageSize.Value
            : 20;

         var temp = new Paginable<T>(items)
         {
            TotalCount = request.TotalCount,
            Page = page,
            PageSize = pageSize,
            FunctionResults = new List<dynamic>(),
         };

         return temp;
      }
   }
}
