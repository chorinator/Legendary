﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using DocumentFormat.OpenXml.Drawing.Diagrams;

namespace Legendary.Core.Utils
{
   public static class ObjectUtils
   {
      public static bool IsEmptyOrNull(this object collection)
      {
         if (collection == null)
            return true;

         if (collection is string)
         {
            var s = (string)collection;
            return string.IsNullOrWhiteSpace(s);
         }

         if (collection is IEnumerable<object>)
         {
            var enumerator = ((IEnumerable<object>)collection).GetEnumerator();
            return !enumerator.MoveNext();
         }
         return false;
      }

      public static bool IsNotEmptyOrNull(this object collection)
      {
         return !IsEmptyOrNull(collection);
      }

      public static T Get<T>(this object obj)
      {
         try
         {
            return (T)Convert.ChangeType(obj, typeof(T));
         }
         catch (Exception)
         {
            throw new InvalidCastException("No se puede castear {0} a {1}".FormatWith(obj, typeof(T).ToString()));
         }
      }

      public static T GetOrDefault<T>(this object obj)
      {
         try
         {
            return obj.Get<T>();
         }
         catch (Exception)
         {
            return default(T);
         }
      }

      /// <summary>
      /// Function that converts a simple request object to valid uri parameters.
      /// </summary>
      /// <param name="obj">Objeto request</param>
      /// <param name="hasNoOtherParams">true: the first params in the uri includes "?" simbol, false: no "?" simbol.</param>
      /// <returns></returns>
      public static string GetUriFromObjectRequest(this object obj, bool hasNoOtherParams)
      {
         Type type = obj.GetType();
         var props = type.GetProperties();
         var uriValue = hasNoOtherParams ? "?" : string.Empty;
         foreach (var field in props)
         {
            string fieldName = field.Name; // Get string name
            var fieldValue = field.GetValue(obj, null); // Get value

            if (fieldValue.IsNotEmptyOrNull())
            {
               uriValue += fieldName + "=" + fieldValue.ToString() + "&";
            }

         }
         uriValue = uriValue.Remove(uriValue.Length - 1);
         return uriValue;
      }

      public static string UrlEncode(this object o)
      {
         var s = o.SerializeToJSON();
         s = s.Base64Encode();
         s = System.Web.HttpUtility.UrlEncode(s);
         return s;
      }

      public static T UrlDecode<T>(this string s)
      {
         var temp = System.Web.HttpUtility.UrlDecode(s);
         temp = temp.Base64Decode();
         var o = temp.DeserializeFromJSON<T>();
         return o;
      }

      public static IList<T> ConvertToList<T>(this T obj)
      {
         return new List<T> {obj};
      }
   }
}
