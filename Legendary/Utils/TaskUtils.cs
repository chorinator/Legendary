﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Web.Hosting;

namespace Legendary.Core.Utils
{
   public static class TaskUtils
   {
      //Ejecuta un Task con un timeout. Si el timeout llega a su fin y el Task no ha terminado se devuelve default(T)
      //Solo usar para operaciones que no es necesario obtener su valor real
      public static T RunWithTimeout<T>(TimeSpan timeout, Func<T> funcParam)
      {
         var token = new CancellationTokenSource(timeout);
         var taskToExecute = Task.Run(funcParam, token.Token);

         if (!taskToExecute.Wait(timeout))
         {
            token.Cancel();
            return default(T);
         }
         var result = taskToExecute.Result;
         taskToExecute.Dispose();
         return result;
      }

      public static void FireAndForget(this Task task, bool rethrowException = false)
      {
         try
         {
            HostingEnvironment.QueueBackgroundWorkItem(token => task.Start());
         }
         catch (Exception)
         {
            if (rethrowException)
               throw;
         }
      }

      public static void FireAndForget(Action func, bool rethrowException = false)
      {
         try
         {
            HostingEnvironment.QueueBackgroundWorkItem(token => func());
         }
         catch (Exception)
         {
            if (rethrowException)
               throw;
         }
      }
   }
}
