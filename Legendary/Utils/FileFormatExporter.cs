﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClosedXML.Excel;
using ClosedXML.Utils;
using Legendary.Core.FileFormatExporter.Model;
using System.IO;

namespace Legendary.Core.Utils
{
    public static class FileFormatExporter
    {
        public static void Add(this List<ExcelCells> list, string cellAddress, string value, bool bold = false){
            list.Add(new ExcelCells(cellAddress,value,bold));
        }
        public static MemoryStream GetExcelFileMemoryStrem(List<ExcelCells> cellsToSave, string fileName, string workBookName) {
            var wb = new XLWorkbook();
            var ws = wb.AddWorksheet(workBookName);            
            foreach (var cell in cellsToSave) {
                ws.Cell(cell.CellAddress).Value = cell.Value;
                ws.Cell(cell.CellAddress).Style.Font.Bold = cell.FontBold;
                ws.Cell(cell.CellAddress).Style.Font.FontColor = cell.Color;                
            }
            ws.Rows().Style.Alignment.SetWrapText();
            ws.Columns().AdjustToContents();
            var memory = new MemoryStream();
            wb.SaveAs(memory);
            return memory;
        }
    }
}
