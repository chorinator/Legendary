﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DocumentFormat.OpenXml.Bibliography;
using DocumentFormat.OpenXml.Office.CustomUI;
using Legendary.Core.Model;
using Legendary.Core.Model.Request;

namespace Legendary.Core.Utils
{
   public static class EnumerableUtils
   {
      public static string ToString<T>(this IEnumerable<T> list, string separator)
      {
         return list.IsEmptyOrNull()
            ? String.Empty
            : string.Join(separator, list.Select(s => s.ToString()));
      }

      public static IEnumerable<TSource> DistinctBy<TSource, TKey>
         (this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
      {
         var knownKeys = new HashSet<TKey>();
         return source.Where(element => knownKeys.Add(keySelector(element)));
      }

      public static Paginable<T> Paginate<T>(this IEnumerable<T> items, int? page, int? pageSize, PaginatedRequest<T> request)
      {
         if (!page.HasValue || page.Value <= 0)
            page = 1;

         if (!pageSize.HasValue || pageSize.Value <= 0)
            pageSize = 20;

         request.Page = page.Value;
         request.PageSize = pageSize.Value;
         request.TotalCount = items.Count();
         
         return items
            .Skip((page.Value - 1)*pageSize.Value)
            .Take(pageSize.Value)
            .AsPaginable(request);
      }

      public static Paginable<T> AsPaginable<T>(this IEnumerable<T> items, PaginatedRequest<T> request)
      {
         var page = request.Page.HasValue && request.Page.Value > 0
            ? request.Page.Value
            : 1;

         var pageSize = request.PageSize.HasValue && request.PageSize.Value > 0
            ? request.PageSize.Value
            : 20;

         var temp = new Paginable<T>(items.AsQueryable())
         {
            TotalCount = request.TotalCount,
            Page = page,
            PageSize = pageSize,            
         };

         return temp;
      }

      public static T PickRandom<T>(this IEnumerable<T> source)
      {
          return source.PickRandom(1).Single();
      }

      public static IEnumerable<T> PickRandom<T>(this IEnumerable<T> source, int count)
      {
          return source.Shuffle().Take(count);
      }

      public static IEnumerable<T> Shuffle<T>(this IEnumerable<T> source)
      {
          return source.OrderBy(x => Guid.NewGuid());
      }

      public static Paginable<T> Paginate<T>(this IEnumerable<T> items, PaginatedRequest<T> request)
      {
         return Paginate(items, request.Page, request.PageSize, request);
      }

      public static Task<Paginable<T>> PaginateAsync<T>(this IEnumerable<T> items, PaginatedRequest<T> request)
      {
         return Task.Run(() => Paginate(items, request));
      }
   }
}