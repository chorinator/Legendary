﻿namespace Legendary.Core.Utils
{
   public static class NullableUtils
   {
      public static bool HasValidValue<T>(this T? t) where T : struct
      {
         return t.HasValue && !t.Value.Equals(default(T));
      }
   }
}
