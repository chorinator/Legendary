﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.IO;
using System.Runtime.Caching;
using System.Net;

namespace Legendary.Core.Utils
{    
    public static class NetworkUtils
    {
        private static readonly ObjectCache Cache = MemoryCache.Default;
        public static string GetPublicIp() {
            var ip = "";
            var cacheKey = "NetworkUtil_PublicIP";
            if (Cache.Get(cacheKey) != null)
                ip = (string)Cache.Get(cacheKey);
            try
            {
                if (HttpContext.Current != null)
                {
                    string VisitorsIPAddr = string.Empty;
                    if (HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
                    {
                        VisitorsIPAddr = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                    }
                    else if (HttpContext.Current.Request.UserHostAddress.Length != 0)
                    {
                        VisitorsIPAddr = HttpContext.Current.Request.UserHostAddress;
                    }
                    ip = VisitorsIPAddr.Replace("\n", "");
                }
                else {
                    String strHostName = string.Empty;
                    // Getting Ip address of local machine...
                    // First get the host name of local machine.
                    strHostName = Dns.GetHostName();                    
                    // Then using host name, get the IP address list..
                    IPHostEntry ipEntry = Dns.GetHostEntry(strHostName);
                    IPAddress[] addr = ipEntry.AddressList;
                    ip = addr[0].ToString().Replace("\n", "");
                }

            }
            catch (Exception)
            {
                ip = "0.0.0.0";
            }
            if (ip != "0.0.0.0")
                Cache.Set(cacheKey, ip, DateTime.Now.AddHours(1));
            return ip;
        }
    }
}
