﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Legendary.Core.Utils
{
   public static class ValidationUtils
   {
      public static IEnumerable<ValidationResult> Validate<T>(ValidationContext validationContext) where T : class
      {
         var current = validationContext.ObjectInstance as T;
         if (current == null)
            throw new InvalidCastException("Object is not of type" + typeof(T));

         var errors = new List<ValidationResult>();
         Validator.TryValidateObject(current, validationContext, errors);

         return errors;
      }

      public static bool IsValid<T>(T obj) where T : class
      {
         var context = new ValidationContext(obj);
         return !Validate<T>(context).Any();
      }

      public static IEnumerable<ValidationResult> Validate<T>(this T obj) where T : class
      {
         var context = new ValidationContext(obj, null, null);
         return Validate<T>(context);
      }
   }
}
