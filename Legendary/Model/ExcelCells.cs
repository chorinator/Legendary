﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClosedXML.Excel;

namespace Legendary.Core.FileFormatExporter.Model
{
    public class ExcelCells
    {
        public ExcelCells() { 
        }
        public ExcelCells(string cellAddress, string value, bool bold = false) {
            CellAddress = cellAddress;
            Value = value;
            FontBold = bold;
        }
        public string CellAddress { get; set; }
        public string Value { get; set; }
        public bool FontBold { get; set; }
        public XLColor Color = XLColor.Black;
        public XLAlignmentHorizontalValues HorizontalFormat { get; set; }
    }
}
