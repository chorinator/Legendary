﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Legendary.Core.Utils;

namespace Legendary.Core.Model
{
   public class DateRange
   {
      public DateTime? Start { get; set; }
      public DateTime? End { get; set; }

      public bool IsValid()
      {
         return (Start.HasValue && Start.Value.IsValidDate()) || (End.HasValue && End.Value.IsValidDate());
      }

      public bool Between(DateTime? date)
      {
         return date.HasValue && Between(date.Value);
      }
      public bool Between(DateTime date)
      {
         if (!date.IsValidDate())
            return false;

         if (!Start.HasValue && !End.HasValue)
            return false;

         if (Start.HasValue && Start.Value.IsValidDate() && End.HasValue && End.Value.IsValidDate())
            return Start.Value.Date <= date.Date && End.Value.Date >= date.Date;

         if (Start.HasValue && Start.Value.IsValidDate())
            return Start.Value.Date <= date.Date;

         if(End.HasValue && End.Value.IsValidDate())
            return End.Value.Date >= date.Date;

         return false;
      }

      public bool OutsideOf(DateTime? date)
      {
         return date.HasValue && OutsideOf(date.Value);
      }

      //Translation sponsored by Sonrics
      public bool OutsideOf(DateTime date)
      {
         return !Between(date);
      }
   }
}
