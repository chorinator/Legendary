﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;
using Legendary.Core.Model.DBRequest;

namespace Legendary.Core.Model.Request
{
   [Serializable]
   public abstract class PaginatedRequest
   {
      public int? Page { get; set; }
      public int? PageSize { get; set; }
      public long TotalCount { get; set; }
   }

   public abstract class PaginatedRequest<T> : PaginatedRequest
   {
      public OrderByRequest<T> OrderBy { get; set; }
      public SumRequest<T> Sum { get; set; }

      public abstract IQueryable<T> ExecuteRequest(IQueryable<T> original);

      public Task<IQueryable<T>> ExecuteRequestAsync(IQueryable<T> original)
      {
         return Task.Run(() => ExecuteRequest(original));
      }

      public List<dynamic> ExecuteOtherFunctions(IQueryable<T> original)
      {
         var list = new List<dynamic>();
         
         if (Sum != null)
         {
            dynamic sum = new ExpandoObject();
            sum.Alias = Sum.Alias;
            sum.Result = Sum.ExecuteRequest(original);
            list.Add(sum);
         }

         return list;
      }
   }
}
