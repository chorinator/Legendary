using System;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using Legendary.Core.Utils;

namespace Legendary.Core.Model.DBRequest
{
   public class DateRequest<T> : DbRequestProperty<T>
   {
      public DateTime Date { get; set; }
      public Enums.DateSearchOption SearchOption { get; set; }
      public bool? TruncateTime { get; set; }

      protected override bool IsValid()
      {
         return SearchOption != Enums.DateSearchOption.None && Date.IsValidDate();
      }

      protected override IQueryable<T> Execute(IQueryable<T> original, string columnName)
      {
         var parameter = Expression.Parameter(typeof(T));
         var property = Expression.PropertyOrField(parameter, columnName);
         var constant = GetConstant();
         var equals = Equals(constant, property);
         Expression predicate;

         switch (SearchOption)
         {
            case Enums.DateSearchOption.Equals:
               predicate = equals;
               break;

            case Enums.DateSearchOption.DateIsGreater:
               predicate = Expression.GreaterThan(constant, property);
               break;

            case Enums.DateSearchOption.DateIsGreaterOrEquals:
               predicate = Expression.Or(Expression.GreaterThan(constant, property), equals);
               break;

            case Enums.DateSearchOption.DateIsLesser:
               predicate = Expression.LessThan(constant, property);
               break;

            case Enums.DateSearchOption.DateIsLesserOrEquals:
               predicate = Expression.Or(Expression.LessThan(constant, property), equals);
               break;

            default:
               throw new NotImplementedException(SearchOption.ToString());
         }

         var lambda = Expression.Lambda<Func<T, bool>>(predicate, parameter);

         return original.Where(lambda);
      }

      private ConstantExpression GetConstant()
      {
         return TruncateTime.HasValue && TruncateTime.Value
            ? Expression.Constant(Date.Date, typeof(DateTime))
            : Expression.Constant(Date, typeof(DateTime));
      }

      private Expression Equals(Expression constant, Expression property)
      {
         if (!TruncateTime.HasValue || !TruncateTime.Value)
            return Expression.Equal(constant, property);

         var before = Expression.GreaterThanOrEqual(property, constant);
         var after = Expression.LessThan(property, Expression.Constant(Date.AddDays(1).Date, typeof(DateTime)));
         return Expression.And(before, after);
      }

      protected override Exception ThrowWhenInvalid()
      {
         if (!Date.IsValidDate())
            throw new InvalidDataException("Date must be a valid date.");

         if (SearchOption == Enums.DateSearchOption.None)
            throw new InvalidDataException("SearchOption cannot be None.");

         throw new NotImplementedException();
      }
   }
}