﻿using System;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using Legendary.Core.Utils;

namespace Legendary.Core.Model.DBRequest
{
   public class StringRequest<T> : DbRequestProperty<T>
   {
      public string Name { get; set; }
      public Enums.StringSearchOption SearchOption { get; set; }

      protected override bool IsValid()
      {
         return !string.IsNullOrWhiteSpace(Name) && SearchOption != Enums.StringSearchOption.None;
      }

      protected override IQueryable<T> Execute(IQueryable<T> original, string columnName)
      {
         var parameter = Expression.Parameter(typeof(T));
         var property = Expression.PropertyOrField(parameter, columnName);
         var constant = Expression.Constant(Name);

         Expression predicate;
         switch (SearchOption)
         {
            case Enums.StringSearchOption.Contains:
               predicate = Expression.Call(property, "Contains", null, constant);
               break;

            case Enums.StringSearchOption.Equals:
               predicate = Expression.Equal(property, constant);
               break;

            case Enums.StringSearchOption.EndsWith:
               predicate = Expression.Call(property, "EndsWith", null, constant);
               break;

            case Enums.StringSearchOption.StartsWith:
               predicate = Expression.Call(property, "StartsWith", null, constant);
               break;

            default:
               throw new NotImplementedException(SearchOption.ToString());
         }

         var lambda = Expression.Lambda<Func<T, bool>>(predicate, parameter);

         return original.Where(lambda);
      }

      protected override Exception ThrowWhenInvalid()
      {
         if (string.IsNullOrWhiteSpace(Name))
            throw new InvalidDataException("Name cannot be null nor empty");

         if (SearchOption == Enums.StringSearchOption.None)
            throw new InvalidDataException("SearchOption cannot be None");

         throw new NotImplementedException();
      }
   }
}
