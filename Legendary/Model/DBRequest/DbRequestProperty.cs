using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace Legendary.Core.Model.DBRequest
{
   public abstract class DbRequestProperty
   {
      protected abstract bool IsValid();
      protected abstract Exception ThrowWhenInvalid();
   }

   public abstract class DbRequestProperty<T> : DbRequestProperty
   {
      protected abstract IQueryable<T> Execute(IQueryable<T> db, string columnName);

      public IQueryable<T> ExecuteRequest(IQueryable<T> original, string columnName)
      {
         if (IsValid())
            return Execute(original, columnName);

         throw ThrowWhenInvalid();
      }

      public IQueryable<T> ExecuteRequestFor<TQ>(IQueryable<T> original, Expression<Func<T, TQ>> expression)
      {
         var name = expression.Body as MemberExpression;
         if (name == null)
            throw new InvalidCastException(typeof(TQ).ToString());

         return ExecuteRequest(original, name.Member.Name);
      }
   }
}