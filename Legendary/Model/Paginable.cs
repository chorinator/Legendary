﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DocumentFormat.OpenXml.Bibliography;
using Newtonsoft.Json;

namespace Legendary.Core.Model
{
   public class Paginable
   {
      [JsonProperty]
      public long TotalCount { get; set; }
      [JsonProperty]
      public int PageSize { get; set; }
      [JsonProperty]
      public int Page { get; set; }
      [JsonProperty]
      public IEnumerable<dynamic> FunctionResults { get; set; }

   }
   public class Paginable<T> : Paginable
   {
      public readonly IEnumerable<T> Items;

      public Paginable(IEnumerable<T> items)
      {
         Items = items;
      }

      public IEnumerable<T> AsEnumerable()
      {
         return Items.AsEnumerable();
      }

      public IEnumerable<T> AsQueryable()
      {
         return Items.AsQueryable();
      }
   }
}
