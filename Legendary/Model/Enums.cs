﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Legendary.Core.Model
{
   public class Enums
   {
      public enum Currency
      {
         None = 0,
         MXN = 1,
         USD = 2,
      }

      public enum DateSearchOption
      {
         None = 0,
         DateIsGreater = 1,
         DateIsLesser = 2,
         Equals = 3,
         DateIsGreaterOrEquals = 4,
         DateIsLesserOrEquals = 5,
      }

      public enum DateRangeSearchOption
      {
         None = 0,
         Between = 1,
         Outside = 2,
      }

      public enum StringSearchOption
      {
         None = 0,
         Equals = 1,
         StartsWith = 2,
         EndsWith = 3,
         Contains = 4,
      }

      public enum OrderBy
      {
         None = 0,
         Desc = 1,
         Asc = 2,
      }
   }
}
