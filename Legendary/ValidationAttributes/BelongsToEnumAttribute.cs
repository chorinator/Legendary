﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Legendary.Core.ValidationAttributes
{
   [AttributeUsage(AttributeTargets.Property | AttributeTargets.Parameter | AttributeTargets.Field, AllowMultiple = false)]
   public class BelongsToEnumAttribute : ValidationAttribute
   {
      public BelongsToEnumAttribute(Type enumType)
      {
         EnumType = enumType;
      }

      public override bool IsValid(object value)
      {
         if (EnumType == null)
         {
            throw new InvalidOperationException("Type cannot be null");
         }
         if (!EnumType.IsEnum)
         {
            throw new InvalidOperationException("Type must be an enum");
         }
         return Enum.IsDefined(EnumType, value);
      }

      private Type EnumType
      {
         get;
         set;
      }
   }
}