﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Legendary.Core.ValidationAttributes
{
   [AttributeUsage(AttributeTargets.Property | AttributeTargets.Parameter, AllowMultiple = false)]
   public class NotDefaultValue : ValidationAttribute
   {
      public override bool IsValid(object value)
      {
         var type = value.GetType();
         if (!type.IsValueType) return true;

         var defaultValue = Activator.CreateInstance(type);
         return !defaultValue.Equals(value);
      }
   }
}

